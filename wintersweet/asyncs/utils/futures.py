import asyncio

from wintersweet.asyncs.interface import RunnableInterface
from wintersweet.utils.base import Utils


class RunAbleFuture(asyncio.Future, RunnableInterface):

    def __init__(self, tag, cro_func, func_args=None, func_kwargs=None):
        super(RunAbleFuture, self).__init__()
        self._tag = tag or Utils.uuid.uuid1().hex
        self._cro_func = cro_func
        self._func_args = func_args or []
        self._func_kwargs = func_kwargs or {}
        self._running = False

    @property
    def tag(self):
        return self._tag

    async def run(self):
        try:
            result = await self._cro_func(*self._func_args, **self._func_kwargs)
            self.set_result(result)
        except Exception as e:
            self.set_exception(e)



