
from aiohttp.hdrs import METH_ALL


class HttpInterface(object):
    """
    接口定义
    """

    def __init__(self, method, uri):
        self._method = method.upper()
        self._uri = uri
        if self._method not in METH_ALL:
            raise ValueError("Method: '%s' not support, please refer to 'aiohttp.hdrs.METH_ALL' !" % self._method)

    @property
    def method(self):
        return self._method

    @property
    def uri(self):
        return self._uri

    def format_interface(self, **kwargs):
        return self.__class__(self._method, self._uri.format(**kwargs))

    def __str__(self):
        return self.__class__.__name__ + '({method}, {uri})'.format(method=self._method, uri=self._uri)
