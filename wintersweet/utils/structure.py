

class Node:

    def __init__(self, _key, _val, _prev=None, _next=None):
        self.key = _key
        self.val = _val
        self.prev = _prev
        self.next = _next

    def __str__(self):
        return f'Node({self.key}, {self.val})'


class EmptyStrPrintType(str):
    def __str__(self):
        return ''

    def __bool__(self):
        return False


class EmptyNonePrintType:
    def __str__(self):
        return 'None'

    def __bool__(self):
        return False


class EmptyIntPrintType:
    def __str__(self):
        return '0'

    def __bool__(self):
        return False


class ContextPrintType:

    def __init__(self, ctx):
        self._ctx = ctx

    def __str__(self):
        return self._ctx.get()

    def __bool__(self):
        return bool(self._ctx.get())
