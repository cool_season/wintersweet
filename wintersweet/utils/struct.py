# -*- coding: utf-8 -*-


class ImmutableDict(dict):
    """
    不可修改字典
    """

    def __init__(self, _dict):
        super(ImmutableDict, self).__init__(_dict)

    def __setitem__(self, key, value):
        raise NotImplementedError('"{}" object can not be modified'.format(self.__class__.__name__))

    def __delitem__(self, key):
        raise NotImplementedError('"{}" object can not be modified'.format(self.__class__.__name__))

    def __str__(self):
        return '{}({})'.format(self.__class__.__name__, dict(self))

    def clear(self):

        raise NotImplementedError('"{}" object can not be modified'.format(self.__class__.__name__))

    def pop(self, key, default=None):

        raise NotImplementedError('"{}" object can not be modified'.format(self.__class__.__name__))

    def popitem(self):

        raise NotImplementedError('"{}" object can not be modified'.format(self.__class__.__name__))

    def setdefault(self, key, default=None):

        raise NotImplementedError('"{}" object can not be modified'.format(self.__class__.__name__))

    def update(self, *args, **kwargs):

        raise NotImplementedError('"{}" object can not be modified'.format(self.__class__.__name__))

