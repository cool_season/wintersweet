DEBUG = True
SECRET = f'__WinterSweet_{DEBUG}_@5806fecc-93b3-11ec-beda-e2b55ff3da64__'

LISTEN_PORT = 8080

# process
PROCESS_NUM = 2

APP_CONFIG = {
    'debug': DEBUG,
    'routes': [],
}

LOGGING_CONFIG = {
    'level': 'debug',
    'file_path': '',
    'file_rotation': None,
    'file_retention': None,
    # 'log_format': DEFAULT_TRACE_LOG_FORMAT
}

# MIDDLEWARES
MIDDLEWARES = [
    # {
    #     'cls': RequestIDMiddleware,
    #     'options': {}
    # },
]

EXCEPTION_HANDLERS = [
    # {
    #     'cls': HTTPException,
    #     'handler': http_exception_handler
    # },
]

DATABASES = {
    # 'default': {
    #     'echo': True,
    #     'host': "localhost",
    #     'user': 'root',
    #     'password': "",
    #     'db': 'test',
    #     'port': 3306,
    #     'charset': r'utf8',
    #     'autocommit': True,
    #     'cursorclass': aiomysql.DictCursor,
    # }
}

REDIS_CONFIG = {
    # 'default': {
    #     'address': "redis://localhost:6379",
    #     'password': None,
    #     'db': 0,
    #     'encoding': 'utf-8',
    #     'minsize': 10
    # }
}

ES_CONFIG = {

}

INTERVAL_TASKS = [
    # (3600, func)
]

CRONTAB_TASKS = [
    # (*, 3, *, *, *, func)
]

NTP_CONFIG = {
    # 'interval': 3600,
    # 'version': 2,
    # 'port': 'ntp',
    # 'timeout': 5,
    # 'hosts': ['ntp2.aliyun.com', 'ntp3.aliyun.com', 'ntp4.aliyun.com', 'ntp5.aliyun.com']
}
