import os
import sys
import uvicorn

os.chdir(os.path.dirname(__file__))
sys.path.insert(0, os.path.abspath(r'../../'))

os.environ.setdefault('WINTERSWEET_SETTINGS_MODULE', 'examples.setting')

from wintersweet.framework.conf import settings
from wintersweet.framework.fastapi.app import Launcher, DEFAULT_HEADERS
from wintersweet.utils.base import Utils


if __name__ == r'__main__':
    app = Launcher.create_app()
    Utils.log.warning(r'THE PRODUCTION ENVIRONMENT IS STARTED USING GUNICORN')

    uvicorn.run(app, port=settings.LISTEN_PORT, log_config=None, headers=DEFAULT_HEADERS)
