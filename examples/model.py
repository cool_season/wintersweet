import sqlalchemy
import sqlalchemy as sa

user_sa = sa.Table(
    r'user', sqlalchemy.MetaData(),
    sa.Column(r'id', sa.Integer, primary_key=True),
    sa.Column(r'username', sa.String, comment=r"username"),
    comment=r"user"
)
